EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Timer:NE555D U1
U 1 1 60B883D7
P 3800 3650
F 0 "U1" H 3800 4231 50  0000 C CNN
F 1 "NE555D" H 3800 4140 50  0000 C CNN
F 2 "Package_SO:SOIC-8_3.9x4.9mm_P1.27mm" H 4650 3250 50  0001 C CNN
F 3 "http://www.ti.com/lit/ds/symlink/ne555.pdf" H 4650 3250 50  0001 C CNN
	1    3800 3650
	1    0    0    -1  
$EndComp
$Comp
L Device:R_Small_US R1
U 1 1 60B89A15
P 4600 3400
F 0 "R1" H 4668 3446 50  0000 L CNN
F 1 "150k" H 4668 3355 50  0000 L CNN
F 2 "" H 4600 3400 50  0001 C CNN
F 3 "~" H 4600 3400 50  0001 C CNN
	1    4600 3400
	1    0    0    -1  
$EndComp
$Comp
L Device:R_Small_US R2
U 1 1 60B8A4BD
P 4600 3750
F 0 "R2" H 4668 3796 50  0000 L CNN
F 1 "620k" H 4668 3705 50  0000 L CNN
F 2 "" H 4600 3750 50  0001 C CNN
F 3 "~" H 4600 3750 50  0001 C CNN
	1    4600 3750
	1    0    0    -1  
$EndComp
$Comp
L Device:C_Small C1
U 1 1 60B8ADC8
P 4600 4150
F 0 "C1" H 4692 4196 50  0000 L CNN
F 1 "47microF" H 4692 4105 50  0000 L CNN
F 2 "" H 4600 4150 50  0001 C CNN
F 3 "~" H 4600 4150 50  0001 C CNN
	1    4600 4150
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 60B8B992
P 4600 4400
F 0 "#PWR?" H 4600 4150 50  0001 C CNN
F 1 "GND" H 4605 4227 50  0000 C CNN
F 2 "" H 4600 4400 50  0001 C CNN
F 3 "" H 4600 4400 50  0001 C CNN
	1    4600 4400
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 60B8C261
P 3800 4150
F 0 "#PWR?" H 3800 3900 50  0001 C CNN
F 1 "GND" H 3805 3977 50  0000 C CNN
F 2 "" H 3800 4150 50  0001 C CNN
F 3 "" H 3800 4150 50  0001 C CNN
	1    3800 4150
	1    0    0    -1  
$EndComp
$Comp
L 4xxx:4017 U1
U 1 1 60B8DBB7
P 5800 3650
F 0 "U1" H 5800 4631 50  0000 C CNN
F 1 "4017" H 5800 4540 50  0000 C CNN
F 2 "" H 5800 3650 50  0001 C CNN
F 3 "http://www.intersil.com/content/dam/Intersil/documents/cd40/cd4017bms-22bms.pdf" H 5800 3650 50  0001 C CNN
	1    5800 3650
	1    0    0    -1  
$EndComp
$Comp
L Device:C_Small C4
U 1 1 60B8ED8E
P 4950 2750
F 0 "C4" H 5042 2796 50  0000 L CNN
F 1 "10microF" H 5042 2705 50  0000 L CNN
F 2 "" H 4950 2750 50  0001 C CNN
F 3 "~" H 4950 2750 50  0001 C CNN
	1    4950 2750
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 60B8F7FD
P 4950 2900
F 0 "#PWR?" H 4950 2650 50  0001 C CNN
F 1 "GND" H 4955 2727 50  0000 C CNN
F 2 "" H 4950 2900 50  0001 C CNN
F 3 "" H 4950 2900 50  0001 C CNN
	1    4950 2900
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 60B8FE33
P 5800 4750
F 0 "#PWR?" H 5800 4500 50  0001 C CNN
F 1 "GND" H 5805 4577 50  0000 C CNN
F 2 "" H 5800 4750 50  0001 C CNN
F 3 "" H 5800 4750 50  0001 C CNN
	1    5800 4750
	1    0    0    -1  
$EndComp
Wire Wire Line
	4600 4250 4600 4400
Wire Wire Line
	4600 3850 4600 3900
Wire Wire Line
	4600 3500 4600 3600
Wire Wire Line
	4300 3850 4550 3850
Wire Wire Line
	4550 3850 4550 3900
Wire Wire Line
	4550 3900 4600 3900
Connection ~ 4600 3900
Wire Wire Line
	4600 3900 4600 4000
Wire Wire Line
	4300 3650 4550 3650
Wire Wire Line
	4550 3650 4550 3600
Wire Wire Line
	4550 3600 4600 3600
Connection ~ 4600 3600
Wire Wire Line
	4600 3600 4600 3650
Wire Wire Line
	3800 4050 3800 4150
Wire Wire Line
	3300 3450 3150 3450
Wire Wire Line
	3150 3450 3150 4100
Wire Wire Line
	3150 4100 4450 4100
Wire Wire Line
	4450 4100 4450 4000
Wire Wire Line
	4450 4000 4600 4000
Connection ~ 4600 4000
Wire Wire Line
	4600 4000 4600 4050
Wire Wire Line
	3800 3250 3800 2550
Wire Wire Line
	4950 2550 4950 2650
Wire Wire Line
	4950 2850 4950 2900
Wire Wire Line
	4600 3300 4600 2550
Connection ~ 4600 2550
Wire Wire Line
	4600 2550 4950 2550
Wire Wire Line
	4300 3450 4400 3450
Wire Wire Line
	3300 3850 3200 3850
Wire Wire Line
	3200 3850 3200 2550
Wire Wire Line
	3200 2550 3800 2550
Connection ~ 3800 2550
Wire Wire Line
	3800 2550 4600 2550
Wire Wire Line
	4400 3450 4400 3150
$Comp
L power:GND #PWR?
U 1 1 60BB0D91
P 5300 3300
F 0 "#PWR?" H 5300 3050 50  0001 C CNN
F 1 "GND" H 5300 3300 50  0000 C CNN
F 2 "" H 5300 3300 50  0001 C CNN
F 3 "" H 5300 3300 50  0001 C CNN
	1    5300 3300
	1    0    0    -1  
$EndComp
Wire Wire Line
	5300 3250 5300 3300
$Comp
L Device:R_Small_US R3
U 1 1 60BBDC49
P 5200 3900
F 0 "R3" H 5268 3946 50  0000 L CNN
F 1 "10k" H 5268 3855 50  0000 L CNN
F 2 "" H 5200 3900 50  0001 C CNN
F 3 "~" H 5200 3900 50  0001 C CNN
	1    5200 3900
	1    0    0    -1  
$EndComp
Wire Wire Line
	4400 3150 5300 3150
$Comp
L Device:C_Small C2
U 1 1 60BBD0FA
P 5200 3600
F 0 "C2" H 5292 3646 50  0000 L CNN
F 1 "10microF" H 5292 3555 50  0000 L CNN
F 2 "" H 5200 3600 50  0001 C CNN
F 3 "~" H 5200 3600 50  0001 C CNN
	1    5200 3600
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 60BBFBF3
P 5200 4150
F 0 "#PWR?" H 5200 3900 50  0001 C CNN
F 1 "GND" H 5205 3977 50  0000 C CNN
F 2 "" H 5200 4150 50  0001 C CNN
F 3 "" H 5200 4150 50  0001 C CNN
	1    5200 4150
	1    0    0    -1  
$EndComp
Wire Wire Line
	4950 2550 5200 2550
Wire Wire Line
	5200 2550 5200 3450
Connection ~ 4950 2550
Wire Wire Line
	5200 3700 5200 3800
Wire Wire Line
	5200 4000 5200 4150
Connection ~ 5200 2550
Wire Wire Line
	5300 3450 5200 3450
Connection ~ 5200 3450
Wire Wire Line
	5200 3450 5200 3500
Wire Wire Line
	5800 4550 5800 4750
$Comp
L Device:R_Small_US R4
U 1 1 60BE87EC
P 6450 4350
F 0 "R4" H 6518 4396 50  0000 L CNN
F 1 "1k" H 6518 4305 50  0000 L CNN
F 2 "" H 6450 4350 50  0001 C CNN
F 3 "~" H 6450 4350 50  0001 C CNN
	1    6450 4350
	1    0    0    -1  
$EndComp
$Comp
L Device:LED D1
U 1 1 60BE9811
P 6450 4700
F 0 "D1" V 6489 4582 50  0000 R CNN
F 1 "LED" V 6398 4582 50  0000 R CNN
F 2 "" H 6450 4700 50  0001 C CNN
F 3 "~" H 6450 4700 50  0001 C CNN
	1    6450 4700
	0    -1   -1   0   
$EndComp
$Comp
L power:GND #PWR?
U 1 1 60BEA9A6
P 6450 5000
F 0 "#PWR?" H 6450 4750 50  0001 C CNN
F 1 "GND" H 6455 4827 50  0000 C CNN
F 2 "" H 6450 5000 50  0001 C CNN
F 3 "" H 6450 5000 50  0001 C CNN
	1    6450 5000
	1    0    0    -1  
$EndComp
Wire Wire Line
	6300 4250 6450 4250
Wire Wire Line
	6450 4450 6450 4550
Wire Wire Line
	6450 4850 6450 5000
$Comp
L Device:D D4
U 1 1 60BF710A
P 6600 4050
F 0 "D4" H 6600 3833 50  0000 C CNN
F 1 "D" H 6600 3924 50  0000 C CNN
F 2 "" H 6600 4050 50  0001 C CNN
F 3 "~" H 6600 4050 50  0001 C CNN
	1    6600 4050
	-1   0    0    1   
$EndComp
$Comp
L Device:D D5
U 1 1 60BF7AC0
P 6600 3550
F 0 "D5" H 6600 3333 50  0000 C CNN
F 1 "D" H 6600 3424 50  0000 C CNN
F 2 "" H 6600 3550 50  0001 C CNN
F 3 "~" H 6600 3550 50  0001 C CNN
	1    6600 3550
	-1   0    0    1   
$EndComp
$Comp
L Device:R_Small_US R5
U 1 1 60BF835E
P 7000 4250
F 0 "R5" H 7068 4296 50  0000 L CNN
F 1 "1k" H 7068 4205 50  0000 L CNN
F 2 "" H 7000 4250 50  0001 C CNN
F 3 "~" H 7000 4250 50  0001 C CNN
	1    7000 4250
	1    0    0    -1  
$EndComp
$Comp
L Device:LED D2
U 1 1 60BF94CD
P 7000 4600
F 0 "D2" V 7039 4482 50  0000 R CNN
F 1 "LED" V 6948 4482 50  0000 R CNN
F 2 "" H 7000 4600 50  0001 C CNN
F 3 "~" H 7000 4600 50  0001 C CNN
	1    7000 4600
	0    -1   -1   0   
$EndComp
$Comp
L power:GND #PWR?
U 1 1 60BFA5CD
P 7000 4950
F 0 "#PWR?" H 7000 4700 50  0001 C CNN
F 1 "GND" H 7005 4777 50  0000 C CNN
F 2 "" H 7000 4950 50  0001 C CNN
F 3 "" H 7000 4950 50  0001 C CNN
	1    7000 4950
	1    0    0    -1  
$EndComp
Wire Wire Line
	6300 3550 6450 3550
Wire Wire Line
	6300 4050 6450 4050
Wire Wire Line
	6750 4050 6850 4050
Wire Wire Line
	7000 4050 7000 4150
Wire Wire Line
	7000 4350 7000 4450
Wire Wire Line
	7000 4750 7000 4950
Wire Wire Line
	6750 3550 6850 3550
Wire Wire Line
	6850 3550 6850 4050
Connection ~ 6850 4050
Wire Wire Line
	6850 4050 7000 4050
$Comp
L Device:D D9
U 1 1 60C144B5
P 7350 4000
F 0 "D9" H 7350 4100 50  0000 C CNN
F 1 "D" H 7350 4200 50  0000 C CNN
F 2 "" H 7350 4000 50  0001 C CNN
F 3 "~" H 7350 4000 50  0001 C CNN
	1    7350 4000
	-1   0    0    1   
$EndComp
$Comp
L Device:D D8
U 1 1 60C14F0A
P 7350 3800
F 0 "D8" H 7250 3800 50  0000 C CNN
F 1 "D" H 7250 3900 50  0000 C CNN
F 2 "" H 7350 3800 50  0001 C CNN
F 3 "~" H 7350 3800 50  0001 C CNN
	1    7350 3800
	-1   0    0    1   
$EndComp
$Comp
L Device:D D7
U 1 1 60C15B4E
P 7350 3600
F 0 "D7" H 7200 3550 50  0000 C CNN
F 1 "D" H 7200 3650 50  0000 C CNN
F 2 "" H 7350 3600 50  0001 C CNN
F 3 "~" H 7350 3600 50  0001 C CNN
	1    7350 3600
	-1   0    0    1   
$EndComp
$Comp
L Device:D D6
U 1 1 60C16A0D
P 7350 3400
F 0 "D6" H 7350 3183 50  0000 C CNN
F 1 "D" H 7350 3274 50  0000 C CNN
F 2 "" H 7350 3400 50  0001 C CNN
F 3 "~" H 7350 3400 50  0001 C CNN
	1    7350 3400
	-1   0    0    1   
$EndComp
$Comp
L Device:R_Small_US R6
U 1 1 60C17FCB
P 7800 4200
F 0 "R6" H 7868 4246 50  0000 L CNN
F 1 "1k" H 7868 4155 50  0000 L CNN
F 2 "" H 7800 4200 50  0001 C CNN
F 3 "~" H 7800 4200 50  0001 C CNN
	1    7800 4200
	1    0    0    -1  
$EndComp
$Comp
L Device:LED D3
U 1 1 60C18F78
P 7800 4600
F 0 "D3" V 7839 4482 50  0000 R CNN
F 1 "LED" V 7748 4482 50  0000 R CNN
F 2 "" H 7800 4600 50  0001 C CNN
F 3 "~" H 7800 4600 50  0001 C CNN
	1    7800 4600
	0    -1   -1   0   
$EndComp
$Comp
L power:GND #PWR?
U 1 1 60C19F7F
P 7800 5000
F 0 "#PWR?" H 7800 4750 50  0001 C CNN
F 1 "GND" H 7805 4827 50  0000 C CNN
F 2 "" H 7800 5000 50  0001 C CNN
F 3 "" H 7800 5000 50  0001 C CNN
	1    7800 5000
	1    0    0    -1  
$EndComp
Wire Wire Line
	6300 3950 7100 3950
Wire Wire Line
	7100 3950 7100 4000
Wire Wire Line
	7100 4000 7200 4000
Wire Wire Line
	6300 3850 7100 3850
Wire Wire Line
	7100 3850 7100 3800
Wire Wire Line
	7100 3800 7200 3800
Wire Wire Line
	6300 3750 7050 3750
Wire Wire Line
	7050 3750 7050 3600
Wire Wire Line
	7050 3600 7200 3600
Wire Wire Line
	6300 3650 6950 3650
Wire Wire Line
	6950 3650 6950 3400
Wire Wire Line
	6950 3400 7200 3400
Wire Wire Line
	7500 3400 7800 3400
Wire Wire Line
	7800 3400 7800 3600
Wire Wire Line
	7500 3600 7800 3600
Connection ~ 7800 3600
Wire Wire Line
	7800 3600 7800 3800
Wire Wire Line
	7500 3800 7800 3800
Connection ~ 7800 3800
Wire Wire Line
	7800 3800 7800 4000
Wire Wire Line
	7500 4000 7800 4000
Connection ~ 7800 4000
Wire Wire Line
	7800 4000 7800 4100
Wire Wire Line
	7800 4300 7800 4450
Wire Wire Line
	7800 4750 7800 5000
$Comp
L Device:Battery Vsupply
U 1 1 60D1844A
P 5500 2450
F 0 "Vsupply" H 5608 2496 50  0000 L CNN
F 1 "5v" H 5608 2405 50  0000 L CNN
F 2 "" V 5500 2510 50  0001 C CNN
F 3 "~" V 5500 2510 50  0001 C CNN
	1    5500 2450
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 60D19C3F
P 5500 2750
F 0 "#PWR?" H 5500 2500 50  0001 C CNN
F 1 "GND" H 5505 2577 50  0000 C CNN
F 2 "" H 5500 2750 50  0001 C CNN
F 3 "" H 5500 2750 50  0001 C CNN
	1    5500 2750
	1    0    0    -1  
$EndComp
Wire Wire Line
	5200 2100 5500 2100
Wire Wire Line
	5500 2100 5500 2250
Wire Wire Line
	5200 2100 5200 2550
Wire Wire Line
	5500 2650 5500 2750
Text Notes 8700 6850 0    50   ~ 0
LED Status Output
$EndSCHEMATC
